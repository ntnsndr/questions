% Software

Research about the uses of questions in various software, online platforms, and social networks.

## Overall themes

* Esp. in @wang-2013, contextual relationship and reciprocity is important for ensuring good question/answer practice


## Education

### @heilman-2010

* "We address  the  challenge  of  automatically generating questions  from  reading  materials for educational practice and assessment. Our approach is to  overgenerate  questions,  then rank them. ... These questions are then ranked by a logistic regression model trained on a small, tailored  dataset  consisting  of  labeled  output from our system.  Experimental results show that ranking nearly doubles the percentage of questions  rated  as  acceptable  by  annotators"
* "since  many  phenomena  pertaining  to question generation are not so easily encoded with rules,  we  include  statistical  ranking  as  an  integral component. Thus, we employ an overgenerate-and-rank approach"
* Identifies question-generation as an under-studied task
* "fifteen  native  English-speaking  university  students rated a set of questions produced from steps 1 and 2, indicating whether each question exhibited any of the deficiencies listed in Table 1. If a question exhibited no deficiencies, raters were asked to label it "acceptable."" This was used as training data for the algorithmic ranker.
* Concludes that "a statistical ranker trained from a small set of annotated questions can capture trends related to question quality that are not easily encoded with rules."

## Quora

### @chen-2018

* A Quora "Top Writer" for 2013--2016 and "Top Question Writer" for 2016--2018
* brief self-description: "multithreaded messenger. Seeks timeless context-independence" and the more detailed part begins:

> I'm a big fan of self-directed learning, upside risk, reducing people’s passivity/perfunctory actions, reducing repetitiveness/redundancies (getting people to repeat themselves less), concepts that change everything, maximizing the amount of high-autonomy time Schmidhuberian agents have in the universe, non-(zero sum) games, accumulative experiences, existence proofs, reducing intergenerational inferential distance, abundance mindsets, expanding people's imagination of what's possible, finding high-energy people, environments that don’t criminalize disengagement, maximizing optionality, non-additive reward/"gratefulness"/"anger" functions, and databases that reduce the need for data wrangling. I like contributing to collective intelligences. I'm also INTP. I find everything (especially unknown unknowns) incredibly fascinating. Ultimately, I'd like to secure a [timeless](http://www.gwern.net/About) context (and path)-independent existence.

* Lists a number of the platforms he uses (often with the handle "simfish") and then explains:

> One of my objectives is to try to make sure that everything I do is as reproducible and as searchable as possible. If you've done it 10 years ago and forgot what you did, it's as good as you not doing it (given how incredibly fallible human memory is). Furthermore, limited cognitive bandwidth means that the number of thoughts you can process each second only becomes a smaller and smaller fraction of the total number of thoughts you've ever held as you age (which is also why it's a good idea to reduce the working memory load of everything you do).
> 
> I also try to post as much raw output as possible since someday, there might be a 14-year old who has the time to spend looking at everything I post (though I know that I have to separate the main points from the "messy details"). I also try to reduce the activation energy it takes for me (and others) to do anything - even some of my silly Quora questions are designed for that purpose. I like to maximize the chances of some remixing happening, and I try to scout for ways where I can create genuine value. Ideally, I’d also like to make everyone around me more audacious.
> 
> In general, I prefer highly generalizable answers that address the chronic (or very long-term) aspects of things, and answers that also address Black Swans (or the very long tail of distributions) or 6-Sigma events.
> 
> I'm very neophilic and neotenous. I avoid virtue-signalling.

* Then follows with a long list of quite specific interests (5-HT2A signalling, PayPal Mafia, etc) and an almost entirely male list of people he finds "highly interesting" and a list of favorite phrases:

> Phrases I like: fine/coarse grained, sensitive/specific, anaphora/cataphora, ad hoc, generalizable, robust, "is it helpful", "show, don't tell", decouple, first-principles thinking, pedagogically suboptimal [Vipul Naik's term], consequence-free failure [Brian Farley's term], information hygiene [Brian Farley's term], satisfice, qualify/hedge, relentlessly resourceful, "cognitive fit", "pressured speech", future-proof, "charitable interpretation", unknown unknowns, reconciling large-scale statistical approaches with small-scale mechanistic approaches (applies in bio + climate science!), think-aloud protocol, distributed cognition, social discovery, increasing contrast between environments, production-over-consumption, non-transitive logics


### @maity-2015

* "As of September 2013, most of the Quora traffic (33.3%) comes from India followed by US (26.5%) and UK (3.8%)"
* Created a four-year dataset using crawlers
* "we propose a prediction framework to predict the popularity of the topics and discuss the important features driving the popularity of a topic. We achieve a high correlation between the predicted value and the ground-truth popularity"


### @wang-2013

* "We shed light on the impact of three different connection networks (or graphs) inside Quora, a graph connecting topics to users, a social graph connecting users, and a graph connecting related questions. Our results show that heterogeneity in the user and question graphs are significant contributors to the quality of Quora's knowledge base. One drives the attention and activity of users, and the other directs them to a small set of popular and interesting questions."
* Notes the relative failures of Google Answers and Yahoo Answers. Quora differs from them by integrating a social network.
* Stack Overflow is much larger, at least in part because it is older
* "We find that while traditional topic-followers generate traffic, social relationships help bring a significant amount of answers to questions, and generally provide much higher quality answers than strangers."
* "We gathered our Quora dataset through web-based crawls between August and early September 2012. We tried to follow crawler-etiquette defined in Quora's robots.txt. Limited portions of data were embedded in Ajax calls. We used FireWatir, an open-source Ruby library, to control a FireFox browser object, simulating clicking and scrolling operations to load the full page."
* Where "degree" constitutes the extent to which Quora deems a question related to other questions, "questions with high degree in the question-relation graph correlates strongly to questions that receive more attention and answers from users."


## Stack Overflow

### @calefato-2017

* "we investigate how information seekers can increase the chance of eliciting a successful answer to their questions on Stack Overflow by focusing on the following actionable factors: affect, presentation quality, and time." Studying 87K questions. Reputation is a control. "We found that regardless of user reputation, successful questions are short, contain code snippets, and do not abuse with uppercase characters. As regards affect, successful questions adopt a neutral emotional style."
* "Asaduzzaman et al. [4] showed that unclear and vague questions remain unanswered in Stack Overflow. Instead, the presence of example code snippets in questions is positively associated with their success [4,17], especially in the case of code reviews [57,64]. Besides, research has recently begun to investigate linguistic aspects too, by looking at how lexicon [37] and narratives [2] in help requests may influence their success. The investigation on human factors has been restrained to analyzing the effect of people's expertise and degree of involvement in the community, suggesting as social reputation [2,3] and personality [6] play a role in the success of online requests."
* "Calefato et al. [8] analyzed the use of affective lexicon in four non-technical Q&A sites of the Stack Exchange network (Mathematics, Bitcoin, Arqade and Science Fiction). Their study reveals that the effect of sentiment and gratitude expressions on the success of a question may vary depending on the community being analyzed."
* "Althoff et al. [2] found that expressing gratitude upfront in Reddit is positively correlated with the success of requests because is seen as a clue of positive disposition towards a potential answerer."

### @ravi-2014

* "we show that there is a notion of question quality that goes *beyond* mere popularity. We present techniques using latent topic models to automatically predict the quality of questions based on their content. Our best system achieves a prediction accuracy of 72%, beating out strong baselines by a significant amount."
* "there has been very little work on understanding the quality of a question, i.e., what would make a question great?"
* Compared to the Yahoo! Answers studies, "our prediction task uses StackOverflow questions, where there is a well-defined notion of up votes and down votes for questions; this enables us to define question quality in a less subjective, principled, and automatic fashion for a large number of questions." Compared to @Li-2017, also, they focus on the quality of content, not just external indicators such as asker reputation. Also, "rather than identifying questions that are of interest to certain answerers, we want to identify questions that are appreciated by people (not just those who are qualified to answer) who are interested in this subject"
* They note SO's own guidance on what constitutes a good question: "question shows research effort; it is useful and clear." But they create their own quality metric based on number of upvotes on a question divided by the number of page views.
* They train a learning algorithm to predict question quality.
* "one might expect that a clearly presented question will tend to be longer than a question with poor quality, but in our dataset, good questions actually tended to be slightly shorter"
* "good questions, even if they started out less popular, prosper as time goes on. / In other words, we argue that good questions have a longer life compared to bad questions of similar popularity. ... The bad questions peak earlier, while good ones peak later, i.e., good questions are more likely to have a sustained and longer life time." This, they argue, is evidence that it is possible to identify question quality in the first place.

### @skeet-2010

* "The Golden Rule: Imagine You're Trying To Answer The Question":

> Once you've finished writing your question, read it through. Imagine you were coming to it fresh, with no context other than what's on the screen. Does it make sense? Is it clear what's being asked? Is it easy to read and understand? Are there any obvious areas you'd need to ask about before providing an answer? You can usually do this pretty well however stuck you are on the actual question. Just apply common sense. If there's anything wrong with the question when you're reading it, obviously that will be a problem for whoever's actually trying to answer it. So fix the problems. Improve the question until you can read it and think, "If I only knew the answer to the question, it would be a pleasure to provide that answer." At that point, post and wait for the answers to come rolling in.

* Emphasizes the importance of context and clarity about one's situation.
* "Make sure it's obvious what you're trying to get out of the question. Too many "questions" are actually just statements: when I do X, something goes wrong. / Well, what did you expect it to do? What are you trying to accomplish? What have you already tried? What happened in those attempts? Be detailed: in particular, if something didn't work, don't just state that: tell us how it failed. If it threw an exception, what was the exception?"

> One trap that many posters fall into is to ask how to achieve some “small” aim, but never say what the larger aim is. Often the smaller aim is either impossible or rarely a good idea – instead, a different approach is needed. Again, if you provide more context when writing your problem statement, we can suggest better designs. It’s fine to specify how you’re currently trying to solve your bigger problem, of course – that’s likely to be necessary detail – but include the bigger goal too.

* This is controversial, but he's on the side of encouraging code examples.
* On naming:

> Register as a user and give yourself a meaningful name. It doesn’t have to be your real name, but frankly names like “Top Coder” or “Coding Guru” look pretty silly when you’re asking a question which others find simple. That’s still better than leaving yourself as “user154232” or whatever identifier is assigned to you by default though. Aside from anything else, it shows a certain amount of commitment to the question and/or site: if you’ve bothered to give yourself a name, you’re less likely to be an “ask-and-run” questioner.

* And then politeness:

> Above all, be polite. Remember that no-one is getting paid to answer your question. Users are giving up their time to help you – so please be appreciative of that. If you’re asking a homework question, explain why you’re asking for help with something that traditionally you’d have to answer all by yourself. If a user suggests that your general approach is wrong and that there’s a better way of doing things, don’t take it personally: they’re trying to help you improve your code. By all means disagree robustly, but don’t start into ad hominem arguments. (This advice applies to answerers as well, of course.)


## Twitter

### @paul-2013

* "we found that the most popular question type on Twitter was rhetorical (42%), followed by factual knowledge (16%), and polls (15%)"
* "Some of these factual questions were likely hard to answer using search engines since they were highly contextual, such as "Can I still book grad photos?" or pertained to real-time events, such as "Where's these protests? Are they like everywhere in England? Or just one place?""
* "In general, the follow relationship on Twitter has low reciprocity; only 22% of relationships are mutual while 78% are one-way (Kwak et al. 2010). We found higher mutual reciprocity among question askers and answerers; 36% of relationships were reciprocal and 55% were one-way"
* "more relevant responses are received when there is a mutual relationship between askers and answerers."


## Yahoo! Answers

### @liu-2017

* "Previous works analyzed the Answer Quality (AQ) based on answer-related features, but neglect the question-related features on AQ"
* "we provide suggestions on how to create a question that will receive high AQ, which can be exploited to improve the QoS of CQAS"
* "Question Quality (QQ) is defined as "well-formedness, readability, utility and interestingness" [3], and refers to its ability to attract users' attention [22], gain answering attempts, and improve the response timeliness and quality of the best answer."
* "We also find the greatest factors in creating a high quality question; most notably the inclusion of "additional details", "category matching", and "Rep. rank of best answer"."
