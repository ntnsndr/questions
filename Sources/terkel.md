% Studs Terkel
% Radio personality, oral historian, and author

@grele1991, 12:

"the phrase that explodes"---kind of rambling passage on interviewing as jazz or as a story, and the nimbleness. Not sure it's quotable.

@grele1991, 31--34:

> Well, I have an analogy. It occurred to me about six months ago, or something like that. The analogy is: a prospector for gold in the days of '49, and what they did. First of all they travelled, they leave for a place unknown, never been there before. This is 1849. I'm talking about the Forty-Niners, right? I'm a prospector for gold. And reading about: either through folk songs, or history, or Mark Twain; the days of 49, the Forty-Niners, the guys who go. What do you do? You go to an unknown, head out, as Huck Finn; head out for the territory. That's what I do; head out for the territory. The guys head out for a place unknown. Then they use some divining rod or whatever they use. ...
> 
> Here's the prospector digging for gold. You're talking, you're probing; something comes out. And there it is: the ore! ... [and then more about mining out the jewels from much much more the people say]
