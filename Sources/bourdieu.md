% Pierre Bourdieu
% Sociologist (1930--2002)

@bourdieu1999

See also: Fowler, Bridget. "[An Introduction to Pierre Bourdieu's 'Understanding'](http://journals.sagepub.com/doi/abs/10.1177/026327696013002001)." _Theory, Culture & Society_ 13, no. 2 (May 1996).

p. 607:

> I would like to avoid pedantic disquisitions on hermeneutics or the "ideal communication situation": indeed, I believe that there is no more real or more realistic way of exploring communication in general than by focusing on the simultaneously practical and theoretical problems that emerge from the particular interaction between the investigator and the person being questioned.
>
> For all that, I do not believe that it is useful to turn to the innumerable so-called "methodological" writings on interview techniques. Useful as these may be when they describe the various effects that the interviewer can produce without knowing it, they almost always miss the point, not least because they remain faithful to old methodological principles which, like the ideal of the standardized procedures, often derived from the desire to imitate the external signs of the rigor of the most established scientific disciplines. At any rate it does not seem to me that they do justice to what has always been done---and known---by researchers who have the most respect for their object and who are the most attentive to the almost infinitely subtle strategies that social agents deploy in the ordinary conduct of their lives.

p. 610, an insert in the text:

> one only needs to have conducted an interview once to become conscious of how difficult it is to focus continuously on what is being said (and not solely in words) and think ahead to questions which might fall "naturally" into the flow of conversation, all the while following a kind of theoretical "line." This means that nobody is immune to the "intrusion effect" created by naively egocentric or, quite simply, inattentive questions and, above all, the fact that answers extorted in this way risk rebounding on the analyst: their interpretation is always liable to take seriously an artefact [sic.] that they themselves have manufactured without knowing it. Thus, to give an example, despite being both a considerate and attentive person, a researcher puts a point-blank question to a steelworker who has just recounted what good luck he has had to stay in the same workshop all his life, asking if he "personally" is "ready to leave Longwy," and gets in reply, once the first moment of complete amazement has passed, a polite response of the type that the hurried researcher and opinion poll coder will categorize as acquiescence: "Now [amazed tone]? Why? Leave? I don't quite see the point of it…No, I don't think I will leave Longwy…That has just never occurred to me…Especially since my wife is still working. That holds me back perhaps…But, leave Longwy? I don't know, maybe, why not? Some day…I won't say "never" but it hasn't occurred to me, particularly as I've stayed…I don't know, why not [laughs], I don't know, you never know…"

p. 614:

> at the risk of shocking both the rigorous methodologist and the inspired hermeneutic scholar, I would say that the interview can be considered a sort of *spiritual exercise* that, through *forgetfulness of self*, aims at a true *conversion of the way we look at* other people in the ordinary circumstances of life. The welcoming disposition, which leads one to make the respondent's problems one's own, the capacity to take that person and understand them just as they are in their distinctive necessity, is a sort of *intellectual love*: a gaze that consents to necessity in the manner of the "intellectual love of God," that is, of the natural order, which Spinoza held to be the supreme form of knowledge.

p. 621, on the subject of "hostility to foreigners" among working-class interviewees:

> The desire to discover the truth, which is constituitive of scientific intent, is totally devoid of any practical power unless it is rendered as a "craft," itself the embodied product of all earlier research and quite other than an abstract, purely intellectual way of knowing. This craft is a real "disposition to pursue truth" (*hexis tou alétheuein*, as Aristotle says in the _Metaphysics_), which disposes one to improvise on the spot, in the urgency of the interview, strategies of self-presentation and adaptive responses, encouragement and opportune questions, etc., so as to help respondents deliver up their truth or, rather, to be delivered of it.
