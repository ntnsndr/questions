# How to Ask Questions

This is a repository for a project-in-progress about the art of question-asking.

Content is written in Pandoc Markdown and BibTeX. References beginning with "@" refer to sources listed in `src.bib`.
